# GDScript formatter

A plugin for Godot that integrates Scony's [GDScript formatter](https://github.com/Scony/godot-gdscript-toolkit) inside the Godot editor.

## Features

- A button to format the current script, located on the top-right of the editor. Only visible when editing a GDScript.
- A shortcut to format the current script (default Ctrl+Alt+L). It can be changed by clicking the "Set format shortcut" on the top-right of the editor.

## Requirements

- The `gdtoolkit` python module, which requires `pytohn3` and can be installed via `pip` using `pip install gdtoolkit`.

## Known issues and limitations

- Formatting a script resets the cursor position to the start of the file